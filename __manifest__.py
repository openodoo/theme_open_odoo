{
    # Theme information
    'name': "theme_open_odoo",
    'description': """
    """,
    'category': 'Theme',
    'version': '1.0',
    'depends': ['website','website_forum'],

    # templates
    'data': [
        'views/assets.xml',
        'views/commons.xml',
        'views/header.xml',
        'views/website_forum.xml',
        # 'views/options.xml',
        # 'views/snippets.xml',
    ],

    # demo pages
    'demo': [
        # 'demo/pages.xml',
    ],

    # Your information
    'author': "Udelta",
    'website': "https://udelta.ru",
}