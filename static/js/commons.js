/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./commons.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./commons.js":
/*!********************!*\
  !*** ./commons.js ***!
  \********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _js_my_stcripts_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./js/my-stcripts.js */ \"./js/my-stcripts.js\");\n/* harmony import */ var _js_my_stcripts_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_js_my_stcripts_js__WEBPACK_IMPORTED_MODULE_0__);\n ///////////\n// Стили //\n///////////\n\n__webpack_require__(/*! ./style/main.scss */ \"./style/main.scss\"); //////////////\n// Картинки //\n//////////////\n\n\n__webpack_require__(/*! ./images/logo_open-odoo.svg */ \"./images/logo_open-odoo.svg\");\n\n__webpack_require__(/*! ./images/logo_open-odoo_white.svg */ \"./images/logo_open-odoo_white.svg\"); /////////////\n// Скрипты //\n/////////////\n// Мои скрипты\n\n\n /////////////////////////////////\n// Загрузка иконок для спрайта //\n/////////////////////////////////\n// main\n\n__webpack_require__(/*! ./icons/arrow-down.svg */ \"./icons/arrow-down.svg\");\n\n__webpack_require__(/*! ./icons/arrow-up.svg */ \"./icons/arrow-up.svg\");\n\n__webpack_require__(/*! ./icons/menu.svg */ \"./icons/menu.svg\");\n\n__webpack_require__(/*! ./icons/home.svg */ \"./icons/home.svg\");\n\n__webpack_require__(/*! ./icons/mail.svg */ \"./icons/mail.svg\");\n\n__webpack_require__(/*! ./icons/phone.svg */ \"./icons/phone.svg\");\n\n__webpack_require__(/*! ./icons/place.svg */ \"./icons/place.svg\");\n\n__webpack_require__(/*! ./icons/search.svg */ \"./icons/search.svg\");\n\n__webpack_require__(/*! ./icons/time.svg */ \"./icons/time.svg\");\n\n__webpack_require__(/*! ./icons/comment.svg */ \"./icons/comment.svg\");\n\n__webpack_require__(/*! ./icons/edit.svg */ \"./icons/edit.svg\");\n\n__webpack_require__(/*! ./icons/close.svg */ \"./icons/close.svg\");\n\n__webpack_require__(/*! ./icons/person.svg */ \"./icons/person.svg\");\n\n__webpack_require__(/*! ./icons/mobile.svg */ \"./icons/mobile.svg\");\n\n__webpack_require__(/*! ./icons/live-help.svg */ \"./icons/live-help.svg\");\n\n__webpack_require__(/*! ./icons/link.svg */ \"./icons/link.svg\");\n\n__webpack_require__(/*! ./icons/people.svg */ \"./icons/people.svg\");\n\n__webpack_require__(/*! ./icons/section-menu.svg */ \"./icons/section-menu.svg\");\n\n__webpack_require__(/*! ./icons/question.svg */ \"./icons/question.svg\");\n\n__webpack_require__(/*! ./icons/date.svg */ \"./icons/date.svg\");\n\n__webpack_require__(/*! ./icons/eye.svg */ \"./icons/eye.svg\");\n\n__webpack_require__(/*! ./icons/answer.svg */ \"./icons/answer.svg\");\n\n__webpack_require__(/*! ./icons/forum.svg */ \"./icons/forum.svg\");\n\n__webpack_require__(/*! ./icons/done.svg */ \"./icons/done.svg\");\n\n__webpack_require__(/*! ./icons/star.svg */ \"./icons/star.svg\");\n\n__webpack_require__(/*! ./icons/reply.svg */ \"./icons/reply.svg\");\n\n__webpack_require__(/*! ./icons/delete.svg */ \"./icons/delete.svg\");\n\n__webpack_require__(/*! ./icons/warning.svg */ \"./icons/warning.svg\");\n\n__webpack_require__(/*! ./icons/chat.svg */ \"./icons/chat.svg\");\n\n__webpack_require__(/*! ./icons/share.svg */ \"./icons/share.svg\");\n\n__webpack_require__(/*! ./icons/cached.svg */ \"./icons/cached.svg\");\n\n__webpack_require__(/*! ./icons/note-on.svg */ \"./icons/note-on.svg\");\n\n__webpack_require__(/*! ./icons/note-off.svg */ \"./icons/note-off.svg\");\n\n__webpack_require__(/*! ./icons/check.svg */ \"./icons/check.svg\");\n\n__webpack_require__(/*! ./icons/undo.svg */ \"./icons/undo.svg\");\n\n__webpack_require__(/*! ./icons/auto-fix.svg */ \"./icons/auto-fix.svg\");\n\n//# sourceURL=webpack:///./commons.js?");

/***/ }),

/***/ "./icons/answer.svg":
/*!**************************!*\
  !*** ./icons/answer.svg ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"answer-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#answer-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/answer.svg?");

/***/ }),

/***/ "./icons/arrow-down.svg":
/*!******************************!*\
  !*** ./icons/arrow-down.svg ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"arrow-down-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#arrow-down-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/arrow-down.svg?");

/***/ }),

/***/ "./icons/arrow-up.svg":
/*!****************************!*\
  !*** ./icons/arrow-up.svg ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"arrow-up-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#arrow-up-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/arrow-up.svg?");

/***/ }),

/***/ "./icons/auto-fix.svg":
/*!****************************!*\
  !*** ./icons/auto-fix.svg ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"auto-fix-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#auto-fix-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/auto-fix.svg?");

/***/ }),

/***/ "./icons/cached.svg":
/*!**************************!*\
  !*** ./icons/cached.svg ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"cached-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#cached-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/cached.svg?");

/***/ }),

/***/ "./icons/chat.svg":
/*!************************!*\
  !*** ./icons/chat.svg ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"chat-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#chat-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/chat.svg?");

/***/ }),

/***/ "./icons/check.svg":
/*!*************************!*\
  !*** ./icons/check.svg ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"check-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#check-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/check.svg?");

/***/ }),

/***/ "./icons/close.svg":
/*!*************************!*\
  !*** ./icons/close.svg ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"close-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#close-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/close.svg?");

/***/ }),

/***/ "./icons/comment.svg":
/*!***************************!*\
  !*** ./icons/comment.svg ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"comment-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#comment-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/comment.svg?");

/***/ }),

/***/ "./icons/date.svg":
/*!************************!*\
  !*** ./icons/date.svg ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"date-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#date-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/date.svg?");

/***/ }),

/***/ "./icons/delete.svg":
/*!**************************!*\
  !*** ./icons/delete.svg ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"delete-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#delete-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/delete.svg?");

/***/ }),

/***/ "./icons/done.svg":
/*!************************!*\
  !*** ./icons/done.svg ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"done-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#done-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/done.svg?");

/***/ }),

/***/ "./icons/edit.svg":
/*!************************!*\
  !*** ./icons/edit.svg ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"edit-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#edit-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/edit.svg?");

/***/ }),

/***/ "./icons/eye.svg":
/*!***********************!*\
  !*** ./icons/eye.svg ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"eye-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#eye-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/eye.svg?");

/***/ }),

/***/ "./icons/forum.svg":
/*!*************************!*\
  !*** ./icons/forum.svg ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"forum-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#forum-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/forum.svg?");

/***/ }),

/***/ "./icons/home.svg":
/*!************************!*\
  !*** ./icons/home.svg ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"home-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#home-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/home.svg?");

/***/ }),

/***/ "./icons/link.svg":
/*!************************!*\
  !*** ./icons/link.svg ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"link-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#link-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/link.svg?");

/***/ }),

/***/ "./icons/live-help.svg":
/*!*****************************!*\
  !*** ./icons/live-help.svg ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"live-help-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#live-help-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/live-help.svg?");

/***/ }),

/***/ "./icons/mail.svg":
/*!************************!*\
  !*** ./icons/mail.svg ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"mail-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#mail-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/mail.svg?");

/***/ }),

/***/ "./icons/menu.svg":
/*!************************!*\
  !*** ./icons/menu.svg ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"menu-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#menu-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/menu.svg?");

/***/ }),

/***/ "./icons/mobile.svg":
/*!**************************!*\
  !*** ./icons/mobile.svg ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"mobile-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#mobile-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/mobile.svg?");

/***/ }),

/***/ "./icons/note-off.svg":
/*!****************************!*\
  !*** ./icons/note-off.svg ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"note-off-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#note-off-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/note-off.svg?");

/***/ }),

/***/ "./icons/note-on.svg":
/*!***************************!*\
  !*** ./icons/note-on.svg ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"note-on-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#note-on-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/note-on.svg?");

/***/ }),

/***/ "./icons/people.svg":
/*!**************************!*\
  !*** ./icons/people.svg ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"people-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#people-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/people.svg?");

/***/ }),

/***/ "./icons/person.svg":
/*!**************************!*\
  !*** ./icons/person.svg ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"person-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#person-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/person.svg?");

/***/ }),

/***/ "./icons/phone.svg":
/*!*************************!*\
  !*** ./icons/phone.svg ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"phone-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#phone-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/phone.svg?");

/***/ }),

/***/ "./icons/place.svg":
/*!*************************!*\
  !*** ./icons/place.svg ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"place-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#place-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/place.svg?");

/***/ }),

/***/ "./icons/question.svg":
/*!****************************!*\
  !*** ./icons/question.svg ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"question-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#question-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/question.svg?");

/***/ }),

/***/ "./icons/reply.svg":
/*!*************************!*\
  !*** ./icons/reply.svg ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"reply-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#reply-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/reply.svg?");

/***/ }),

/***/ "./icons/search.svg":
/*!**************************!*\
  !*** ./icons/search.svg ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"search-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#search-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/search.svg?");

/***/ }),

/***/ "./icons/section-menu.svg":
/*!********************************!*\
  !*** ./icons/section-menu.svg ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"section-menu-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#section-menu-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/section-menu.svg?");

/***/ }),

/***/ "./icons/share.svg":
/*!*************************!*\
  !*** ./icons/share.svg ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"share-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#share-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/share.svg?");

/***/ }),

/***/ "./icons/star.svg":
/*!************************!*\
  !*** ./icons/star.svg ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"star-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#star-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/star.svg?");

/***/ }),

/***/ "./icons/time.svg":
/*!************************!*\
  !*** ./icons/time.svg ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"time-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#time-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/time.svg?");

/***/ }),

/***/ "./icons/undo.svg":
/*!************************!*\
  !*** ./icons/undo.svg ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"undo-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#undo-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/undo.svg?");

/***/ }),

/***/ "./icons/warning.svg":
/*!***************************!*\
  !*** ./icons/warning.svg ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n      id: \"warning-usage\",\n      viewBox: \"0 0 24 24\",\n      url: __webpack_require__.p + \"icons/icons-sprite.svg#warning-usage\",\n      toString: function () {\n        return this.url;\n      }\n    });\n\n//# sourceURL=webpack:///./icons/warning.svg?");

/***/ }),

/***/ "./images/logo_open-odoo.svg":
/*!***********************************!*\
  !*** ./images/logo_open-odoo.svg ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__.p + \"images/logo_open-odoo.svg\";\n\n//# sourceURL=webpack:///./images/logo_open-odoo.svg?");

/***/ }),

/***/ "./images/logo_open-odoo_white.svg":
/*!*****************************************!*\
  !*** ./images/logo_open-odoo_white.svg ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__.p + \"images/logo_open-odoo_white.svg\";\n\n//# sourceURL=webpack:///./images/logo_open-odoo_white.svg?");

/***/ }),

/***/ "./js/my-stcripts.js":
/*!***************************!*\
  !*** ./js/my-stcripts.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/* WEBPACK VAR INJECTION */(function($) {$(document).ready(function () {\n  if ($('.main__header').attr('data-background-image') != undefined) {\n    var url = $('.main__header').attr('data-background-image');\n    console.log($('.main__header').attr('data-background-image'));\n    $('.main__header').css('background-image', 'url(' + url + ')');\n  }\n}); // Закрытие приветственной панели на форуме\n\n$('.forum_intro__close').click(function (event) {\n  event.preventDefault();\n  console.log('Click close button');\n  var currentHeight = $('.forum_intro').outerHeight();\n  $('.forum_intro').css('height', currentHeight);\n  $('.forum_intro').css('height', '0').css('margin', '0').css('padding', '0').css('overflow', 'hidden').css('opacity', '0');\n});\n$('.js_unfollow_btn').click(function () {\n  $(this).addClass('hidden');\n  $('.js_follow_btn').removeClass('hidden');\n  $('.js_follow_email').removeAttr('disabled');\n});\n$('.js_follow_btn').click(function () {\n  $(this).addClass('hidden');\n  $('.js_unfollow_btn').removeClass('hidden');\n  $('.js_follow_email').attr('disabled', 'disabled');\n}); // Работа кнопки в избранное (Звездочка)\n// $('.favorit-forum-item__link').click(function (event) {\n//     event.preventDefault();\n//     $(this).toggleClass('favorit-forum-item__link_active');\n// });\n\n$('.favorit-forum-item__link').popover(); // Работа кнопки в избранное (Звездочка)\n// $('.accept-answer-forum-item__link').click(function (event) {\n//     event.preventDefault();\n//     $(this).toggleClass('accept-answer-forum-item__link_active');\n// });\n\n$('.accept-answer-forum-item__link').popover(); // Ссылка Дать ответ\n\n$(\"#post-reply-link\").click(function (event) {\n  //отменяем стандартную обработку нажатия по ссылке\n  event.preventDefault(); //узнаем высоту от начала страницы до блока на который ссылается якорь\n\n  var anchor = $('#post_reply').offset().top - 5; //анимируем переход на расстояние - top за 800 мс\n\n  $('body,html').animate({\n    scrollTop: anchor\n  }, 800);\n});\n/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ \"jquery\")))\n\n//# sourceURL=webpack:///./js/my-stcripts.js?");

/***/ }),

/***/ "./style/main.scss":
/*!*************************!*\
  !*** ./style/main.scss ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./style/main.scss?");

/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = jQuery;\n\n//# sourceURL=webpack:///external_%22jQuery%22?");

/***/ })

/******/ });