"use strict";




///////////
// Стили //
///////////

require('./style/main.scss');


//////////////
// Картинки //
//////////////

require('./images/logo_open-odoo.svg');
require('./images/logo_open-odoo_white.svg');

/////////////
// Скрипты //
/////////////

// Мои скрипты
import './js/my-stcripts.js';

/////////////////////////////////
// Загрузка иконок для спрайта //
/////////////////////////////////

// main
require('./icons/arrow-down.svg');
require('./icons/arrow-up.svg');
require('./icons/menu.svg');
require('./icons/home.svg');
require('./icons/mail.svg');
require('./icons/phone.svg');
require('./icons/place.svg');
require('./icons/search.svg');
require('./icons/time.svg');
require('./icons/comment.svg');
require('./icons/edit.svg');
require('./icons/close.svg');
require('./icons/person.svg');
require('./icons/mobile.svg');
require('./icons/live-help.svg');
require('./icons/link.svg');
require('./icons/people.svg');
require('./icons/section-menu.svg');
require('./icons/question.svg');
require('./icons/date.svg');
require('./icons/eye.svg');
require('./icons/answer.svg');
require('./icons/forum.svg');
require('./icons/done.svg');
require('./icons/star.svg');
require('./icons/reply.svg');
require('./icons/delete.svg');
require('./icons/warning.svg');
require('./icons/chat.svg');
require('./icons/share.svg');
require('./icons/cached.svg');
require('./icons/note-on.svg');
require('./icons/note-off.svg');
require('./icons/check.svg');
require('./icons/undo.svg');
require('./icons/auto-fix.svg');


